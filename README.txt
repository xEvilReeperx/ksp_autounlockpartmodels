/******************************************************************************
 *             AutoUnlockPartModels for Kerbal Space Program                  *
 *                                                                            *
 * Version 1.0                                                                *
 * Created: 3/16/2014 by xEvilReeperx                                         *
 * ************************************************************************** *
 
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
 * ***************************************************************************/



------------------------------------------------------------------
-        What does it do?
------------------------------------------------------------------

When new parts are added to a KSP install, new parts in a tech 
node that has already been locked will need to be "purchased".
In the future, this will cost ingame credits but until implemented
is merely an annoyance. This plugin will automatically unlock any
locked part models belonging to researched tech nodes whenever you enter
the the space center.

------------------------------------------------------------------
-        Installation
------------------------------------------------------------------

Place the GameData folder and its contents into your KSP root 
directory.


------------------------------------------------------------------
-        Removal
------------------------------------------------------------------

Delete the AutoUnlockPartModels directory in your KSP/GameData 
folder. You may remove this plugin at any time with no consequences
to your saves.


------------------------------------------------------------------
-        Source
------------------------------------------------------------------

Included.  Released as public domain