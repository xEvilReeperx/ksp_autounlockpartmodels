﻿/******************************************************************************
 *             AutoUnlockPartModels for Kerbal Space Program                  *
 *                                                                            *
 * Version 1.0                                                                *
 * Created: 3/16/2014 by xEvilReeperx                                         *
 * ************************************************************************** *
 
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
 * ***************************************************************************/

using UnityEngine;

namespace AutoUnlockPartModels
{
    [KSPAddon(KSPAddon.Startup.SpaceCentre, false)]
    public class AutoUnlocker : MonoBehaviour
    {
        public void Start()
        {
            StartCoroutine(DelayedStart());
        }

        private System.Collections.IEnumerator DelayedStart()
        {
            while (ResearchAndDevelopment.Instance == null)
                yield return null;

            Debug.Log("Unlocking part models of research nodes that have been purchased.");

            foreach (var part in PartLoader.LoadedPartsList)
            {
                var techNode = ResearchAndDevelopment.Instance.GetTechState(part.TechRequired);

                if (techNode != null)
                    if (ResearchAndDevelopment.GetTechnologyState(techNode.techID) == RDTech.State.Available)
                        if (!ResearchAndDevelopment.PartModelPurchased(part) && !techNode.partsPurchased.Contains(part))
                        {
                            techNode.partsPurchased.Add(part);
                            Debug.Log("Unlocking " + part.title);
                            ResearchAndDevelopment.Instance.SetTechState(techNode.techID, techNode);
                        }
            }

            Debug.Log("Finished unlocking part models.");
        }
    }
}
